# Distressed Digital

Distressed Digital purchases undervalued digital assets or digital asset producing infrastructure.

Distressed Digital is partly owned by the [mizim](http://mizim.city) group.

### Learn More

 + [Pitch deck](http://distressed.digital?toload=/Documents/distresseddigital/slides/investor-pitch.html.aes&loadtoel=content)
 + [Term sheet](http://distressed.digital?toload=/Documents/distresseddigital/legal/term-sheet.md.aes&loadtoel=content)

